import img1 from './img/1.jpg';
import img2 from './img/2.jpg';
import img3 from './img/3.webp';
import img4 from './img/4.webp';
import img5 from './img/5.webp';
import img6 from './img/6.webp';
import img7 from './img/7.jpg';
import img8 from './img/8.webp';
import img9 from './img/9.webp';

export const products = [
  {
    id: 1,
    img: img1,
    link: "#",
  },
  {
    id: 2,
    img: img2,
    link: "#",
  },
  {
    id: 3,
    img: img3,
    link: "#",
  },
  {
    id: 4,
    img: img4,
    link: "#",
  },
  {
    id: 5,
    img: img5,
    link: "#",
  },
  {
    id: 6,
    img: img6,
    link: "#",
  },
  {
    id: 7,
    img: img7,
    link: "#",
  },
  {
    id: 8,
    img: img8,
    link: "#",
  },
  {
    id: 9,
    img: img9,
    link: "#",
  },
];
