import "./toggle.css";
import { useContext } from "react";
import { ThemeContext } from "../../context";
import { GoSun } from "react-icons/go";
import { BsMoon } from "react-icons/bs";

const Toggle = () => {
  const theme = useContext(ThemeContext);

  const handleClick = () => {
    console.log(theme.state.darkMode);
    localStorage.setItem("dark", !theme.state.darkMode)
    theme.dispatch({ type: "TOGGLE" });
  };
  return (
    <div className="t" onClick={handleClick}>
      {!theme.state.darkMode ? <GoSun /> : <BsMoon />}
    </div>
  );
};

export default Toggle;
