import "./intro.css";
import Me from "../../img/me.png";

const Intro = () => {
  return (
    <div className="i">
      <div className="i-left">
        <div className="i-left-wrapper">
          <h2 className="i-intro">Hello, My name is</h2>
          <h1 className="i-name">Krishna Sapkota</h1>
          <div className="i-title">
            <div className="i-title-wrapper">
              <div className="i-title-item">Veterinarian</div>
              <div className="i-title-item">Animal Expert</div>
              <div className="i-title-item">Care Taker</div>
              <div className="i-title-item">Animal Lover</div>
            </div>
          </div>
          <p className="i-desc">
            "Passionate about the well-being of our furry friends, I am a
            dedicated veterinary student on a journey to make a positive impact
            in the world of animal care. With a profound love for animals and a
            commitment to their health, I am continually expanding my knowledge
            and skills to provide compassionate and top-notch veterinary care.
            Explore my portfolio to discover my academic achievements, practical
            experiences, and the unwavering dedication I bring to the field.
            Together, let's create a healthier, happier world for our beloved
            companions."
          </p>
        </div>
        <svg
          width="75"
          height="75"
          viewBox="0 0 75 75"
          fill="none"
          stroke="black"
          className="i-scroll"
          xmlns="http://www.w3.org/2000/svg"
        >
          <g id="scroll">
            <path id="Vector" d="M40.5 15L34.5 9L28.5 15" />
            <path id="Vector_2" d="M28.5 24L34.5 30L40.5 24" />
            <g id="Group">
              <path id="Vector_3" d="M9 37.5H60" />
            </g>
            <path id="Vector_4" d="M34.5 27V9" />
            <g id="Group_2">
              <path
                id="Vector_5"
                d="M9 27C9 12.918 20.418 1.5 34.5 1.5C48.5859 1.5 60 12.918 60 27C60 29.8906 60 45.1094 60 48C60 62.082 48.5859 73.5 34.5 73.5C20.418 73.5 9 62.082 9 48C9 45.1094 9 29.8906 9 27Z"
              />
            </g>
          </g>
        </svg>
      </div>
      <div className="i-right">
        <div className="i-bg"></div>
        <img src={Me} alt="" className="i-img" />
      </div>
    </div>
  );
};

export default Intro;
