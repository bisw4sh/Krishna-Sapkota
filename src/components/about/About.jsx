import "./about.css";
import Award from "../../img/award.webp";
import Hero from '../../img/hero.webp'

const About = () => {
  return (
    <div className="a">
      <div className="a-left">
        <div className="a-card bg"></div>
        <div className="a-card">
          <img src={Hero} alt="" className="a-img" />
        </div>
      </div>
      <div className="a-right">
        <h1 className="a-title">About Me</h1>
        <div className="a-sub">
          <ul className="a-sub">
            <li data-icon="🦄"> B.V.Sc. & A.H.</li>
            <li>Veterinary Student</li>
            <li>IVSA SCAW EDCUATION OFFICER</li>
            <li>IVSA SCOVE Ambassador</li>
            <li>IVSA NPI Exchange Officer</li>
          </ul>
        </div>
        <p className="a-desc">
          I am Krishna Sapkota, a dedicated and experienced veterinarian
          committed to ensuring the health and well-being of our beloved animal
          companions.
        </p>
        <div className="a-award">
          <img src={Award} alt="" className="a-award-img" />
          <div className="a-award-texts">
            <h4 className="a-award-title">IVSA SCOH Ambassador 2022</h4>
            <p className="a-award-desc">I&apos;ve been active as a college representative and passionate individual in this organization.</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
