import { createContext, useReducer } from "react";

export const ThemeContext = createContext();

const Preference = () => {
  const darkLocalStorage = localStorage.getItem("dark");

  if (darkLocalStorage) {
    return darkLocalStorage === "true";
  }

  if (
    window.matchMedia &&
    window.matchMedia("(prefers-color-scheme: dark)").matches
  ) {
    return true;
  }

  return false;
};


const INITIAL_STATE = { darkMode: Preference() };

const themeReducer = (state, action) => {
  switch (action.type) {
    case "TOGGLE":
      return { darkMode: !state.darkMode };
    default:
      return state;
  }
};

export const ThemeProvider = (props) => {
  const [state, dispatch] = useReducer(themeReducer, INITIAL_STATE);

  return (
    <ThemeContext.Provider value={{ state, dispatch }}>
      {props.children}
    </ThemeContext.Provider>
  );
};
